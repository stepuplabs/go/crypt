package crypt

import (
	"crypto/md5"
	"crypto/sha1"
	"crypto/sha256"
	"encoding/base64"
	"fmt"

	"github.com/google/uuid"
)

// UUID - returns an UUID in string format
func UUID() string {

	id, err := uuid.NewRandom()

	if err != nil {
		return ""
	}

	bytes, err := id.MarshalText()

	if err != nil {
		return ""
	}

	return string(bytes)
}

// SHA256 - digests content using SHA256 algorithm
func SHA256(content []byte) string {

	digested := sha256.Sum256([]byte(content))

	return HexStr(digested[:])
}

// SHA1 - digest content using SHA1 algorithm
func SHA1(content []byte) string {

	digested := sha1.Sum(content)

	return HexStr(digested[:])
}

// MD5 - digest content using MD5 algorithm
func MD5(content []byte) string {

	digested := md5.Sum(content)

	return HexStr(digested[:])
}

// Base64Encode - encodes using base64
func Base64Encode(content []byte) string {
	return base64.StdEncoding.EncodeToString(content)
}

// Base64Decode - encodes using base64
func Base64Decode(encoded string) ([]byte, error) {
	return base64.StdEncoding.DecodeString(encoded)
}

// HexStr - converts an slice of byte into a hexadecimal representation
func HexStr(content []byte) string {

	result := ""

	for i := 0; i < len(content); i++ {
		result += fmt.Sprintf("%02x", content[i])
	}

	return result
}

// NewPassword - creates a new encrypted password and salt
func NewPassword(password string) (digestedPassword string, salt string) {

	salt = UUID()
	digestedPassword = Password(password, salt)

	return
}

// Password - Calculates Digested Password
func Password(password string, salt string) (digestedPassword string) {

	digestedPassword = SHA256([]byte(password + "::" + salt))

	return
}

// NewToken - creates a new random and encrypted token
func NewToken() (clearToken string, encryptedToken string) {

	clearToken = SHA256([]byte(UUID()))
	encryptedToken = SHA256([]byte(clearToken))

	return
}
